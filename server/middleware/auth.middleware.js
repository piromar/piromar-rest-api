var jwt = require('jsonwebtoken');

let verifyToken = (req, res, next) => {

    let token = req.get('Authorization');
    if (token) token = token.substring(7, token.length);

    jwt.verify(token, 'piromar@token', (err, decoded) => {
        if (err) return res.status(401).json({ success: false, err: { message: 'Token not valid!' } });
        req.user = decoded.user;
        next();
    });
}

let verifyAdminRole = (req, res, next) => {
    let user = req.user;
    if (user.role !== 'ADMIN_ROLE')
        return res.status(401).json({ success: false, err: { message: 'User not authorized' } });
    next();
}

let verifyImgToken = (req, res, next) => {
    let token = req.query.token;
    jwt.verify(token, 'piromar@token', (err, decoded) => {
        if (err) return res.status(401).json({ success: false, err: { message: 'Token not valid!' } });
        req.user = decoded.user;
        next();
    });
}

module.exports = {
    verifyToken,
    verifyAdminRole,
    verifyImgToken
}