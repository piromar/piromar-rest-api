const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user.model');
const app = express();

//-------------------------------
//   Method login
//-------------------------------
app.post('/login', (req, res) => {

    let body = req.body;

    User.findOne({ email: body.email }, (err, user) => {
        if (err) return res.status(500).json({ ok: false, err });

        if (!user || !bcrypt.compareSync(body.password, user.password)) {
            return res.status(400)
                .json({ ok: false, err: { message: 'El inicio de sesión no es correcto' } });
        }

        let token = jwt.sign({ user }, 'piromar@token', { expiresIn: 60 * 60 * 24 * 30 });

        res.json({
            ok: true,
            user,
            token
        })
    });
});


module.exports = app;