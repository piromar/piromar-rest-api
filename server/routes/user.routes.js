const express = require('express');
const bcrypt = require('bcrypt');
const underscore = require('underscore');
const User = require('../models/user.model');
const { verifyToken, verifyAdminRole } = require('../middleware/auth.middleware');

const app = express();

//-------------------------------
//   Method retrieve all
//-------------------------------
app.get('/user', verifyToken, (req, res) => {

    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 5;
    limit = Number(limit);

    criteria = {
        status: true
    }

    User.find(criteria, 'name email role status img')
        .skip(from)
        .limit(limit)
        .exec((err, users) => {
            if (err) return res.status(500).json({ success: false, err });
            User.count(criteria, (err, total) => {
                if (err) return res.status(500).json({ success: false, err });
                res.json({
                    success: true,
                    users,
                    total
                });
            });
        });
});

//-------------------------------
//   Method retrieve
//-------------------------------
app.get('/user/:id', verifyToken, (req, res) => {

    let id = req.params.id;

    User.findById(id, 'name email role status img')
        .exec((err, userDB) => {
            if (err) return res.status(500).json({ success: false, err });
            if (!userDB) {
                return res.status(400).json({ success: false, err: { message: 'User not found in our system with this id ' + id } });
            }
            res.json({
                success: true,
                userDB
            });

        });
});

//-------------------------------
//   Method create
//-------------------------------
app.post('/user', [verifyToken, verifyAdminRole], (req, res) => {
    let body = req.body;

    console.log(body);

    let user = new User({
        name: body.name,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        role: body.role
    });

    user.save((err, userDB) => {
        if (err) return res.status(500).json({ success: false, err });
        res.status(201).json({
            success: true,
            user: userDB
        })
    });

});

//-------------------------------
//   Method update
//-------------------------------
app.put('/user/:id', [verifyToken, verifyAdminRole], (req, res) => {
    let id = req.params.id;
    let body = underscore.pick(req.body, ['name', 'img', 'role', 'status']);

    User.findByIdAndUpdate(id, body, { new: true, runValidators: true })
        .exec((err, userDB) => {
            if (err) return res.status(500).json({ success: false, err });

            if (!userDB) {
                return res.status(400).json({ success: false, err: { message: 'User not found in our system with this id ' + id } });
            }

            res.status.json({
                success: true,
                user: userDB
            })
        })
});

//-------------------------------
//   Method update status
//-------------------------------
app.put('/user/check/:id', [verifyToken, verifyAdminRole], (req, res) => {
    let id = req.params.id;
    let changeStatus = {
        status: false
    }

    User.findByIdAndUpdate(id, changeStatus, ({ new: true }))
        .exec((err, userDB) => {
            if (err) return res.status(500).json({ success: false, err });

            if (!userDB) {
                return res.status(400).json({ success: false, err: { message: 'User not found in our system with this id ' + id } });
            }

            res.json({
                success: true,
                user: userDB
            });
        });


});

//-------------------------------
//   Method delete
//-------------------------------
app.delete('/user/:id', [verifyToken, verifyAdminRole], (req, res) => {
    let id = req.params.id;

    User.findByIdAndRemove(id, (err, userDB) => {
        if (err) return res.status(500).json({ success: false, err });

        if (!userDB) {
            return res.status(400).json({ success: false, err: { message: 'User not found in our system with this id ' + id } });
        }

        res.json({
            success: true,
            user: userDB
        });
    });

});

module.exports = app;