const mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

let rolesValidate = {
    values: ['ADMIN_ROLE', 'USER_ROLE'],
    message: '{VALUE} not valid role'
}

let Schema = mongoose.Schema;

let userSchema = new Schema({
    name: { type: String, required: ['true', 'Name is need'] },
    email: { type: String, required: ['true', 'Email is need'], unique: true },
    password: { type: String, required: ['true', 'Password is need'] },
    img: { type: String, required: false },
    role: { type: String, default: 'USER_ROLE', enum: rolesValidate },
    status: { type: Boolean, default: true }
});


userSchema.methods.toJSON = function() {
    let user = this;
    let userObjects = user.toObject();
    delete userObjects.password;
    return userObjects;
}

userSchema.plugin(uniqueValidator, { message: '{PATH} should be unique' });

module.exports = mongoose.model('user', userSchema);